import numpy as np

def lorenz63_dxdt(x, a=10.0, b=8.0/3.0, r=28.0):
    dx = np.zeros(3)

    dx[0] = a*(x[1] - x[0])
    dx[1] = -x[0]*x[2] + r*x[0] - x[1]
    dx[2] = x[0]*x[1] - b*x[2]

    return dx
