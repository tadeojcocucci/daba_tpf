import numpy as np
import scipy.spatial.distance as distance
import scipy.stats as stats
import ot
from tqdm import tqdm

def deterministic_resampling(x, w):
    nx, ne = x.shape
    D = distance.cdist(x.T, x.T, 'sqeuclidean')
    T = ot.emd(w, np.ones(ne)/ne, D)
    P = ne * T
    return (P.T @ x.T).T

def ETPF_analysis(xf, likelihood, n_thresh=None):

    nx, ne = xf.shape

    # Compute weights
    likelihood_values = np.array([likelihood(xf[:, j]) for j in range(ne)])
    w = likelihood_values.reshape(ne)
    w /= np.sum(w)

    # Resample
    xa = deterministic_resampling(xf, w)

    return xa

def ETPF(ne, **kwargs):
    
    def assimilator(process, y, desc='ETPF %s'%ne, **kwargs):
        
        X0 = process.X0
        f = process.f
        q = process.q
        h = process.h
        r = process.r
        nx = process.nx
        ncy = process.ncy
        
        xf = np.zeros((nx, ne, ncy+1))
        xa = np.zeros((nx, ne, ncy+1))

        xf[:, :, 0] = X0.sample(ne).T
        xa[:, :, 0] = np.copy(xf[:, :, 0])

        for k in tqdm(range(1, ncy+1), leave=False, desc=desc):
        
            # Evolve particles forward
            for j in np.arange(ne):
                xf[:, j, k] = f(k-1)(xa[:, j, k-1])
            
            # Add noise to forcast
            xf[:, :, k] += q(k).sample(ne).T

            # Compute analysis
            likelihood = lambda x: process.obs_process(k, x).pdf(y[:, k])

            xa[:, :, k] = ETPF_analysis(xf[..., k], likelihood, **kwargs)
        
        return xf, xa

    return assimilator