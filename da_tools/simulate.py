import numpy as np

def simulate_truth_and_obs(process):
    nx = process.nx
    ncy = process.ncy
    m = process.m
    f = process.f
    q = process.q
    h = process.h
    r = process.r

    x_true = np.zeros((nx, ncy+1))
    y = np.zeros((m, ncy+1))

    x_true[:, 0] = process.X0.sample()
    y[:, 0].fill(np.nan)

    for k in np.arange(1, ncy+1):
        x_true[:, k] = f(k-1)(x_true[:, k-1]) + q(k).sample()
        y[:, k] = h(k)(x_true[:, k]) + r(k).sample()

    return x_true, y
