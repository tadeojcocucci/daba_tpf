import numpy as np
from tqdm import tqdm

def EnKF_analysis(xf, y, H, r, infl = 1.0, loc_mask=None):
    nx, ne = xf.shape
    R = r.cov

    if loc_mask is not None:
        S = np.cov(xf) * infl * loc_mask
    else:
        S = np.cov(xf) * infl

    SHT = S @ H.T
    K = SHT @ np.linalg.pinv(H @ SHT + R)

    D = np.vstack(y) + r.sample(ne).T
    innov = D - H @ xf

    xa = xf + K @ innov

    return xa

def EnKF(ne, infl=1.0, analysis_scheme='PertObs'):

    def assimilator(process, y, desc='EnKF%s'%ne):
        X0 = process.X0
        f = process.f
        q = process.q
        h = process.h
        r = process.r
        nx = process.nx
        ncy = process.ncy

        xf = np.zeros((nx, ne, ncy+1))
        xa = np.zeros((nx, ne, ncy+1))

        # Initialization
        xf[:, :, 0] = X0.sample(ne).T
        xa[:, :, 0] = xf[:, :, 0]

        for k in tqdm(np.arange(1, ncy+1), leave=False, desc=desc):
            # Evolve particles forward
            for j in np.arange(ne):
                xf[:, j, k] = f(k-1)(xa[:, j, k-1])
            # Add noise to forcast
            xf[:, :, k] += q(k).sample(ne).T

            # Compute analysis ensemble
            xa[:, :, k] = EnKF_analysis(xf[:, :, k], y[:, k],
                                          h(k).matrix, r(k), infl=infl)

        return xf, xa

    return assimilator
