import numpy as np
import scipy.spatial.distance as distance
import scipy.stats as stats
import ot
from tqdm import tqdm

from random_variables import GaussianRV

def compute_phi(lik_values, n_thresh, phi_old,
                n_precision=0.01, phi_precision=0.01):
    n_eff = 0
    a = phi_old
    b = 1

    while abs(n_thresh-n_eff) > n_precision:
        phi = 0.5 * (b + a)
        w = np.power(lik_values, phi)
        w /= np.sum(w)
        n_eff = 1/(np.sum(w**2))

        if n_eff>n_thresh:
            a = phi
        else:
            b = phi

        if abs(phi-1)<phi_precision:
            phi = 1
            break

        if abs(b-a)<phi_precision:
            break

    return phi

def multinomial_resampling(x, w):
    ne = len(w)
    indexes = np.random.choice(ne, size=ne, p=w)
    return x[:, indexes]

def deterministic_resampling(x, w):
    nx, ne = x.shape
    D = distance.cdist(x.T, x.T, 'sqeuclidean')
    T = ot.emd(w, np.ones(ne)/ne, D)
    P = ne * T
    return (P.T @ x.T).T

def MCMC_mutation(x, likelihood, phi, noise, n_mcmc=10):
    nx, ne = x.shape

    for j in range(ne):
        for k in range(n_mcmc):
            xprop = x[:, j] + noise.sample()
            likelihood_ratio = (np.power(likelihood(xprop), phi) /
                                np.power(likelihood(x[:, j]), phi))

            u = np.random.uniform()
            if u < min(1, likelihood_ratio):
                x[:, j] = xprop

    return x


def pcnMCMC_mutation(x, likelihood, phi, noise,
                     n_mcmc=10, beta=0.045):
    nx, ne = x.shape

    for j in range(ne):
        for k in range(n_mcmc):
            xprop = np.sqrt(1-beta**2) * x[:, j] + beta*noise.sample()
            likelihood_ratio = (np.power(likelihood(xprop), phi) /
                                np.power(likelihood(x[:, j]), phi))

            u = np.random.uniform()
            if u < min(1, likelihood_ratio):
                x[:, j] = xprop

    return x

def TPF_analysis(xf, likelihood, x_prior, n_thresh=None, n_mcmc=10,
                 beta=0.045):

    nx, ne = xf.shape
    xa = np.copy(xf)

    phi = 0
    phi_old = 0

    if n_thresh is None:
        n_thresh = ne // 10

    i = 0
    while True :
        i = i+1

        # Compute likelihood values
        likelihood_values = np.array([likelihood(xa[:, j]) for j in range(ne)])

        # Compute phi
        phi = compute_phi(likelihood_values, n_thresh, phi_old)

        # Compute weights
        w = np.power(likelihood_values, phi-phi_old)
        w = w.reshape(ne)
        w /= np.sum(w)

        # Resample
        xa = deterministic_resampling(xa, w)

        # Mutation
        xa = MCMC_mutation(xa, likelihood, phi, x_prior,
                              n_mcmc=n_mcmc)

        if phi == 1:
            break
        else:
            phi_old = phi
    
    return xa

def tempered_PF(ne, **kwargs):
    
    def assimilator(process, y, desc='TPF %s'%ne, **kwargs):
        
        X0 = process.X0
        f = process.f
        q = process.q
        h = process.h
        r = process.r
        nx = process.nx
        ncy = process.ncy
        
        xf = np.zeros((nx, ne, ncy+1))
        xa = np.zeros((nx, ne, ncy+1))

        xf[:, :, 0] = X0.sample(ne).T
        xa[:, :, 0] = np.copy(xf[:, :, 0])

        for k in tqdm(range(1, ncy+1), leave=False, desc=desc):
        
            # Evolve particles forward
            for j in np.arange(ne):
                xf[:, j, k] = f(k-1)(xa[:, j, k-1])
            
            # Add noise to forcast
            xf[:, :, k] += q(k).sample(ne).T

            # Compute analysis
            likelihood = lambda x: process.obs_process(k, x).pdf(y[:, k])
            x_prior = GaussianRV(0, np.cov(xf[:, :, k]))
            xa[:, :, k] = TPF_analysis(xf[..., k], likelihood, x_prior, **kwargs)
        return xf, xa

    return assimilator