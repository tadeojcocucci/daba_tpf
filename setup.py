import sys
import os
path = os.path.dirname(os.path.realpath(__file__))
path = os.path.abspath(path)
if not path in sys.path:
    sys.path.insert(0, path)
