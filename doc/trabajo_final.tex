\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\oddsidemargin .1cm
\evensidemargin .1cm
\textwidth 16.5cm
\topmargin -2truecm
\textheight 23cm

\RequirePackage{amsmath}
\usepackage[
backend=biber,
bibencoding=utf8,
style=apa,
citestyle=authoryear,
sorting=ynt,
doi=false,
eprint=false,
uniquename=false,
language=spanish
]{biblatex}
\addbibresource{biblio.bib}
\AtBeginBibliography{\small}
\DefineBibliographyStrings{spanish}{andothers = {et\addabbrvspace al\adddot}
}

\usepackage{nameref, hyperref}

% improves typesetting in LaTeX
\usepackage{microtype}
\DisableLigatures[f]{encoding = *, family = * }

% adjust caption style
\usepackage[font=footnotesize,labelfont=bf, ]{caption}
\renewcommand{\figurename}{Figura}

% use \textcolor{color}{text} for colored text (e.g. highlight to-do areas)
\usepackage{color}

\usepackage{graphicx}
\hyphenation{te-rre-nos plan-ta-cio-nes co-rres-pon-dien-te}
\graphicspath{{figs/}}


% use for have text wrap around figures
\usepackage{wrapfig}
\usepackage[pscoord]{eso-pic}
% \usepackage[fulladjust]{marginnote}
% \reversemarginpar

% Commands

% for vectors
\renewcommand{\v}[1]{\ensuremath{\mathbf{#1}}}
% for vectors of Greek letters
\newcommand{\gv}[1]{\ensuremath{\mbox{\boldmath$ #1 $}}}

\begin{document}
\vspace*{1cm}

% Title
\begin{flushleft}
{\Large
\textbf{Filtro de partículas temperado}
}
\newline
\\
% Authors
Tadeo Javier Cocucci, FaMAF-UNC
\end{flushleft}

\section*{Resumen}
En este trabajo se introduce filtro de partículas temperado
(\cite{ruchi_etal_2019, dubinkina2019}) implementado para el
contexto de asimilación de datos
secuencial. Se realizan experimentos sobre el modelo Lorenz-63 para evaluar
su performance y compararla con la del filtro de Kalman por ensembles
estocástico (\cite{burgers_etal_1998}) y el filtro de partículas de ensamble
transformado (\cite{reich_2013}). Los experimentos contemplan distintos errores
observacionales, espaciamiento temporal entre observaciones y sensibilidad a
modelos mal especificados. El código con las implementaciones y los experimentos
está disponible en \url{https://gitlab.com/tadeojcocucci/daba_tpf}

% the * after section prevents numbering
\subsection*{Introducción}
En el contexto de asimilación de datos, las variables de interés (llamado
estado y denominado con la letra $\v x$) evoluciona temporalmente y se dispone
de un modelo que representa esta evolución. A su vez, en ciertos instantes de
tiempo, el estado es observado. Las observaciones (generalmente representadas
con la letra $\v y$) pueden ser directas o indirectas y contamos con un operador
que mapea el espacio del estado en el espacio de la observaciones.
Tanto el proceso de evolución temporal como el
proceso observacional que vincula al estado con
las observaciones están sujetas a ruido. Más formalmente podemos pensar en el
siguiente modelo:
\begin{align*}
\v x_k &= \mathcal{M}_{k-1} (\v x_{k-1}) + \gv \eta_{k} \\
\v y_k &= \mathcal{H}_k (\v x_k) + \gv \nu_{k}
\end{align*}
donde el subíndice $k=1,..., K$ indica los pasos de tiempo, $\v x_k$
(resp. $\v y_k$) es la variable aleatoria que representa al estado (resp. la
observación) en el tiempo $k$. El operador $\mathcal{M}_{k-1}$ es modelo y
$\mathcal{H}_k$ es el operador observacional. $\gv \eta_k$ y $\gv \nu_k$ son
variables aleatorias que representan al error de modelo y al error
observacional respectvamente. Este planteo asume que los errores son
aditivos. Adicionalmente, como condición inicial, se supone cierta
distribución sobre $\v x_0$. Las ecuaciones entonces nos dan la forma de la
distribución de transición $p(\v x_k | \v x_{k-1})$ y la verosimilitud de las
observaciones $p(\v y_k | \v x_k)$.

Sobre este modelo, conocido a veces como \textit{state-space model}, una de
las distribuciones de interés sobre la que se quiere inferir es la distribución
filtrante $p(\v x_k | \v y_{1:k})$ (o de análisis),
es decir la distribución del estado
a tiempo $k$ dado que se ha observado el estado desde el tiempo $0$ hasta el
$k$. Esto usualmente se hace en base a la distribución de pronóstico
$p(\v x_k | \v y_{1:k-1})$.

\subsubsection*{Representación de distribuciones
de probabilidad mediante muestras}
Una forma de representar distribuciones de probabilidad es asumir que pertenecen
a una familia paramétrica e inferir que parámetros son los que mejor se
ajustan a nuestro problema. Típicamente se asume gaussianidad y en ese caso,
basta con inferir sobre la media y covarianza. En asimilación de datos,
debido que generalmente se usan modelos no-lineales, las distribuciones de
pronóstico y filtrantes pueden no estar correctamente representadas con
la aproximación gaussiana y por lo tanto se acude a representarlas mediante
muestras (o conjuntos de partículas).
Las representaciones de distribuciones con muestras permiten
calcular esperanzas mediante aproximaciones de Monte Carlo, es decir que si
tenemos muestras $\{ \v x^{(j)} \}_{j=1}^N \sim p$ entonces podemos hacer la
siguiente aproximación
$$ E_p[f(\v x)] = \int f(\v x) p(\v x) d\v x \simeq
\frac{1}{N} \sum_{j=1}^N \v x^{(j)} $$

Si la muestra no fuera exactamente un muestra de $p$ sino de una distribución
$q$ (usualmente llamada distribución propuesta),
también es posible hacer una aproximación similar (importance sampling):
$$ E_p[f(\v x)] =
\int f(\v x) \frac{p(\v x) q(\v x)} {q(\v x)} d\v x
\simeq \frac{1}{N} \sum_{j=1}^N w^{(j)} \v x^{(j)} $$
donde $w^{(j)} = \frac{p(\v x)} {q(\v x)}$ son los pesos correspondientes.
De esta manera logramos representar la densidad $p$ con la muestra pesada
$\{ (\v x^{(j)}, w^{(j)}) \}_{j=1}^N$.

Esto quiere decir que en el caso de la aproximación de Monte Carlo sin pesos
estamos haciendo la aproximación
$
p(\v x) \simeq \frac{1}{N} \sum_{j=1}^N \delta (\v x^{(j)} - \v x)
$
y en importance sampling
$
p(\v x) \simeq \frac{1}{N} \sum_{j=1}^N w^{(j)} \delta (\v x^{(j)} - \v  x)
$,
donde $\delta$ es la delta de dirac, es decir la distribución que tiene toda la
probabilidad concentrada en el origen.

\subsubsection*{Filtros de partículas}
Se puede utilizar importance sampling de manera secuecial en asimilación de datos
para obtener muestras pesadas de las distribuciones de pronóstico y de análisis.
Suponiendo que tenemos una muestra
$\{ (\v x^{(j)}_{k-1}, w^{(j)}_{k-1}) \}_{j=1}^N$.
que representa la distribución filtrante $p(\v x_{k-1} | \v y_{1:k-1})$ es posible
obtener una muestra del análisis al tiempo $k$ tomando partículas de una
distribución propuesta
$
\v x_k^{(j)} \sim \pi (\v x_k | \v x_{k-1}^{(j)}, \v y_k )$
y actualizado los pesos como
$$
w_k^{(j)} \propto w_{k-1}^{(j)}
\frac{p(\v y_k | \v x_{k}^{(j)}) p(\v x_k^{(j)} | \v x_{k-1}^{(j)})}
{\pi (\v x_k^{(j)} | \v x_{k-1}^{(j)}, \v y_k )}
$$
y normalizándolos para que sumen $1$. El conjunto de partículas actualizado
$\{ (\v x^{(j)}_{k}, w^{(j)}_{k}) \}_{j=1}^N$ termina siendo una muestra de
la distribución filtrante en el tiempo $k$. Este método es llamado
\textit{sequential importance sampling} (SIS). En el caso particular de que se
la distribución propuesta se elija igual a la distribución de transición, ie
$ \pi (\v x_k | \v x_{k-1}^{(j)}, \v y_k ) = p(\v x_k | \v x_{k-1}^{(j)})$,
entonces el cálculo de los pesos se simplifica y solo requiere la evaluación de
la verosimilitud $p(\v y_k | \v x_{k}^{(j)})$ y el muestreo de la distribución
propuesta no es otra cosa que la evolución a través del modelo de las partículas
del tiempo anterior. En este caso particular al filtro de partículas se lo
denomina \textit{bootstrap filter}.

El filtro SIS, tal como está planteado, sufre el llamado \textit{problema de la
degeneración del filtro} que significa que luego de algunas iteraciones, los
pesos son casi todos muy próximos a cero y al normalizarlos unas pocas partículas
se llevan todo o casi todo el peso. Este problema se suele solucionar haciendo
remuestreo periódicamente o cuando sabemos que el filtro comienza a degenerarse.
El remuestreo más común es el multinomial, es decir se extraen muestras con
reemplazo de las partículas en donde la probabilidad de extraer una partícula
está dada por su peso. Luego del remuestreo todos los pesos se fijan en $1/N$.
Esto tiene la consecuencia de que las partículas con mayores pesos son
replicadas mientras que las partículas con menor peso son eliminadas.
Con esta variación el filtro se denomina \textit{sequential importance
resampling} (SIR). Existen otros métodos de remuestreo, en particular el
remuestreo determinístico basado en transporte óptimo que se va a utilizar en
este trabajo consiste en transportar a la distribución multinomial dada por las
partículas pesadas a la distribución dada por las las partículas, pero con
pesos todos iguales a $1/N$. El transporte es óptimo en el sentido que la
covarainza entre las distribuciones es maximizada.

El remuestreo de partículas, mientras que soluciona el problema de la
degeneración del filtro trae consigo el problema de la \textit{pérdida de la
diversidad} o \textit{empobrecimiento} que se da cuando el remuestreo solo
produce copias de unas pocas partículas y por lo tanto no puede representar
correctamente la distribución. Para intentar paliar esta situación se suele
incorporar una fase de mutación de las partículas, luego del remuestreo en el
que las partículas son movidas para incorporar diversidad a la muestra. En
este caso se utilizarán algunos pasos de MCMC basado en una caminata aleatoria
usando un esquema de aceptación Metropolis-Hastings.

En casos en que las observaciones son muy precisas, existe la posibilidad
de que los pesos no puedan representar correctamente la información de la
verosimilitud de la observación. Notemos que para actulizar los pesos es
necesario evaluar la verosimilitud de las partículas, es decir es necesario
computar $p(\v y_k | \v x_{k}^{(j)})$. Cuando la observación es muy precisa
la función de verosimilitud va a tener un máximo muy abrupto y posiblemente los
valores de la verosimilitud de las partículas sea muy cercano a cero.
En \cite{ruchi_etal_2019, dubinkina2019}
se propone que en lugar de transformar las partículas
del pronóstico al análisis en un solo paso, esto se haga de manera paulatina a
través de verosimilitudes temperadas. Se factoriza la verosimilitud como
$
p(\v y_k | \v x_k) = p(\v y_k | \v x_k)^{\phi_1} ... p(\v y_k | \v x_k)^{\phi_n}
$
donde $\phi_1< ...< \phi_n$ son parámetros de temperado que suman $1$ en total.
Se puede interpretar a los pasos $1, ..., n$ como pseudo-tiempos en los que
las partículas se van adaptando a las sucesivas versiones temperadas de la
verosimilitud. Al introducir el temperado las sucesivas verosimilitudes son más
suaves que la original y se van adaptando de manera paulatina a esta
(\cite{vanleeuwen_2019}).
Entonces, se comienza con las partículas del pronósitco y se
las actualiza (como en el esquema SIR) pero usando la primera verosimilitud
$p(\v y_k | \v x_k)^{\phi_1}$, luego las partículas se actualizan usando
sucesivamete la ``diferencia'' entre las verosimilitudes
$$
\frac{p(\v y_k | \v x_k)^{\phi_k}}{p(\v y_k | \v x_k)^{\phi_{k-1}}}
= p(\v y_k | \v x_k)^{\phi_k - \phi_{k-1}}
$$

Los parámetros $\phi_1, ..., \phi_n$ son elegidos en cada pseudo-tiempo para que
el número de partículas efectivas, es decir el número de partículas que tienen
peso significativo, sea igual a un valor de umbral $N_{thresh}$ predefinido. De
esta manera nos aseguramos que la actualización de las partículas a través de
los pasos de temperado sea lo suficientemente paulatina como para que el
número de partículas efectivas no caiga debajo de este umbral. El número de
partículas efectivo depende del parámetro $\phi$ que se elija y puede ser
aproximado como
$$N_{eff}(\phi) = \left[ \sum_{j=1}^{N} (w^{(j)}(\phi))^2 \right]^{-1}$$
donde $w^{(j)}(\phi)$ es el peso para la partícula $j$ proveniente de elegir
$\phi$ como parámetro de temperado. Entonces, en el pseudo-tiempo $k$, $\phi_k$
se elije de manera que solucione la ecuación $N_{eff}(\phi_k) = N_{thresh}$.
Esto es implementado mediante  un algoritmo de bisección sobre el intervalo
$(\phi_{k-1}, 1)$.

\subsection*{Experimentos y resultados}
En base a \cite{ruchi_etal_2019} se implementó un filtro de partículas temperado
en el que la distribución propuesta es la distribución de transición (es
decir es un fitro bootstrap). En cada pseudo-tiempo $k$ se realizan los
siguientes pasos:
\begin{enumerate}
\item Se elige $\phi_k$ mediante un algoritmo de bisección
\item Se computan los pesos, que son iguales a la verosimilitud de cada partícula
normalizados para que sumen $1$
\item Con los pesos se hace resampleo determinístico basado en transporte
óptimo
\item Se establecen todos los pesos como $1/N$
\item Se mutan las partículas mediante MCMC con caminata aleatoria donde cada
paso de esta caminata es sampleado de una ditribución gaussiana con covarainza
igual a la covarianza muestral de las partículas del pronóstico
\end{enumerate}

% \begin{wrapfigure}[14]{l}{7cm}
% \includegraphics[width=7cm]{obs_error.eps}
% \caption{RMSE y cobertura para distintos errores observacionales}
% \label{fig1}
% \end{wrapfigure}
El método (lo llamamos TPF) es evaluado en el
modelo Lorenz-63 y comparado con el EnKF
(\cite{burgers_etal_1998}) y el ETPF \cite{reich_2013}. Se consideran cuatro
experimentos en el que se evalúa la sensibilidad de los métodos en distintas
configuraciones del sistema. En particular estudiamos la performance modificando
el número de partículas, utilizando un modelo mal especificado respecto al
que generó las observaciones, considerando distinto espaciamiento de las
observaciones en el tiempo, cambiando la magnitud del error observacional
y usando diferentes tamaños de ensamble.

Para todos los experimentos el error de modelo es gaussiano con media cero y
covarianza $\v Q = 0.3 \v I$. El operador observacional es $\v H = \v I$,
es decir el estado completo es observado de manera directa en todo tiempo. El
% \begin{wrapfigure}[11]{r}{7cm}
% \includegraphics[width=7cm]{obs_distance.eps}
% \caption{RMSE y cobertura para distintos espaciamientos temporales de las
% observaciones}
% \label{fig2}
% \end{wrapfigure}
error observacional es gaussiano con media cero y covarianza
$\v R = \sigma_R^2 \v I$ con $\sigma_R^2 = 0.3$ para todos los experimentos
(excepto para el experimento en el que se evalúa la sensibilidad a este
parámetro en que se prueban diferentes valores). El modelo dinámico usa los
parámetros estándar del Lorenz-63 salvo que se aclare lo contrario. La
integración numérica se hace con el método Runge-Kutta de cuarto orden y con
una resolución $dt=0.01$. En todos los experimentos la cantidad de pasos de
tiempo entre observaciones es $dk=10$ escepto en el experimento en que se evalúa
la robustez al espaciamiento de las observaciones. En todos los casos se
utilizan 250 ciclos de asimilación y salvo que se exprese lo contrario, se
utilizan 50 partículas para todos los métodos. Para diagnosticar la performance
se computa el error cuadrático medio (RMSE) y la cobertura del estado real en
las bandas de confianza del $95\%$ estimadas bajo suposición gaussiana. Se espera
que si la dispersión de las estimaciones está correctamente cuantificada la
cobertura sea de $0.95$
\begin{figure}[h!]
\centering
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[width=0.9\textwidth]{obs_error.eps}
\caption{RMSE y cobertura para distintos errores observacionales}
\label{fig1}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[width=0.9\textwidth]{obs_distance.eps}
\caption{RMSE y cobertura para distintos espaciamientos temporales de las
observaciones}
\label{fig2}
\end{minipage}
\end{figure}

La figura \ref{fig1} muestra la cobertura y el RMSE obtenidos con los tres
métodos utilizando distintos valores para $\sigma_R^2$. podemos ver que como es
de esperar, cuando las observaciones son más ruidosas el error de la estimación
es mayor. La performance del EnKF es consistentemente mejor pero la cobertura
del ETPF se aproxima más al valor esperado. El ETPF muestra una performance
inferior y diverge para error pequeño, esto puede deberse a que por no usar
temperado las partículas no puedan capturar correctamente la información
de una verosimilitud muy concentrada. El hecho de que la cobertura del EnKF
sea subóptima podría explicarse por la tendencia del filtro a colapsar dado que
no se utilizó inflación.

En la figura \ref{fig2} podemos ver la sensibilidad de los métodos a la distancia
entre observaciones consecutivas. Los valores faltantes corresponden a valores
en los que el filtro divergió. Se puede ver que el ETPF es extremadamente sensible
a este factor y diverge para oservaciones con un distanciamiento mayor a 20
pasos de tiempo. El efecto de observaciones muy separadas es un pronósitco con
mucha varianza y es posible que al no usar temperado el ETPF deba transformar
las partículas con pesos muy mal condicionados. El EnKF es el que tiene un
comportamiento más estable pero mantiene la tendencia a subestimar la varianza.
El TPF es estable en términos de RMSE aunque va empeorando el cubrimiento a mayor
separación y finalmente diverge cuando hay 150 pasos de tiempo entre observación
 y observación.


\begin{figure}
\centering
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[width=0.9\textwidth]{model_error.eps}
\caption{RMSE y cobertura para distintos valores del parámetro a en
las ecuaciones del modelo}
\label{fig3}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[width=0.9\textwidth]{ensemble_size.eps}
\caption{RMSE y cobertura para distintos tamaños de ensamble}
\label{fig4}
\end{minipage}
\end{figure}

Los resultados del experimento para estudiar la sensibilidad al error de modelo
se encuentran en la figura \ref{fig3}. El valor del parámetro con el que se
simularon las observaciones es $a=10$ Podemos ver que el TPF no tiene mucha
sensibilidad al error de modelo, el error y la cobertura son aproximadamente
iguales para las distintas parametrizaciones. El EnKF es sensible
al parámetro en términos de RMSE pero no de cobertura. El ETPF no tiene una
buena performance comparativamente e incluso su mejor performance se da para
un valor incorrecto del parámetro.

En cuanto al tamaño de ensamble, podemos ver en la figura \ref{fig4} que la
performance de todos los métodos mejora con esambles más grandes como es de
esperar. El TPF no es muy sensible al número de partículas que se
utilicen, lo cual puede explicarse porque el resampleo y la mutación
sacan mayor provecho a muestras pequeñas.

\subsection*{Discusión}
Al menos en los experimentos realizados el ENKF muestra mayor robsutez que los
otros métodos y también una performance, en general, superior. Sin embargo, es
notable que el TPF logra una mejor representación de la varianza como muestran
los resultados de la cobertura. El ETPF
no logra una buena performance comparativamente pero la implementación es un
tanto naive y puede ser perfeccionada. Por otro lado, el TPF es mucho más
costoso computacionalmente que los otros dos métodos.
También es importante remarcar que la performance del TPF puede aún ser mejorada
mediante \textit{fine-tuning}, por ejemplo optimizando la cantidad
de pasos de MCMC en la fase de mutación o el valor de $N_{thresh}$.

\clearpage

\printbibliography[heading=subbibliography, title={Referencias}]

\end{document}
