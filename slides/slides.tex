\documentclass{beamer}
\usetheme[progressbar=frametitle]{metropolis}
\setsansfont{Ubuntu}
\setmonofont{Ubuntu Mono}

% metropolis theme requires lualatex or xelatex compilation
\useoutertheme{metropolis}
\useinnertheme{metropolis}
\usefonttheme{metropolis}
\usecolortheme{seahorse}

\makeatletter
\setlength{\metropolis@titleseparator@linewidth}{1.5pt}
\setlength{\metropolis@progressonsectionpage@linewidth}{1.5pt}
\setlength{\metropolis@progressinheadfoot@linewidth}{1.5pt}
\makeatother

\definecolor{myblue}{rgb}{0.07,0.48,0.96}
\definecolor{mylightblue}{rgb}{0.5,0.71,0.95}
\setbeamercolor{frametitle}{bg=mylightblue}
\setbeamercolor{progress bar}{fg=myblue}
\setbeamercolor{background canvas}{bg=white}

% \usecolortheme{seahorse}
% \beamertemplatenavigationsymbolsempty

\usepackage{nameref, hyperref}

% Commands

% for vectors
\renewcommand{\v}[1]{\ensuremath{\mathbf{#1}}}
% for vectors of Greek letters
\newcommand{\gv}[1]{\ensuremath{\mbox{\boldmath$ #1 $}}}


%---------------------------------------------------------
%Title page
\title[]{Estudio de performance del filtro de partículas temperado}
\author{Tadeo Javier Cocucci}
\institute{FaMAF-UNC}
\date{2019}
%---------------------------------------------------------

\begin{document}

%---------------------------------------------------------
\frame{\titlepage}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{State-space model}

Trabajamos con el esquema de datos con error aditivo como en el curso.

\begin{align*}
\v x_k &= \mathcal{M}(\v x_{k-1}) + \gv \eta_k
  && \rightarrow p(\v x_k | \v x_{k-1}) \\
\v y_k &= \mathcal{H}(\v x_k) + \gv \nu_k
  && \rightarrow p(\v y_k | \v x_{k})
\end{align*}

\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Muestras pesadas y con pesos uniformes}
El objetivo general de los fitros de partículas es obtener una muestra que
represente la distribución del análisis (filtrante)
$p(\v x_k | \v y_1, ..., \v y_k)$. \pause

Esta muestra puede ser pesada $\{(\v x_k^{(j)}, w_k^{(j)}) \}_{j=1}^N$ o con
pesos uniformes $\{\v x_k^{(j)}\}_{j=1}^N$.
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Filtro bootstrap}
Vamos a comenzar de una implementación sencilla del filtro de partículas.
Partiendo de una muestra pesada del análisis en el tiempo $k-1$
$$
\{ (\v x_{k-1}^{(j)}, w_{k-1}^{(j)}) \}_{j=1}^N
$$
obtenemos una muestra pesada del análisis al tiempo $k$ haciendo:
\begin{itemize}
\item $x_{k}^{(j)} = \mathcal{M}_{k-1}(\v x_{k-1}^{(j)}) + \gv \eta_k^{(j)}$
\item $w_k^{(j)} \propto w_{k-1}^{(j)} p(\v y_k | \v x_{k}^{(j)})$
\item Periódicamente (!) resamplear y restaurar los pesos a $1/N$
\end{itemize}

\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{SIR}
\centering
\includegraphics[scale=0.55]{SIR_plot.pdf}
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Por qué resampleo?}
Si el pronóstico es muy amplio y la verosimilitud muy ``picuda''
vamos a terminar con unas pocas partículas que se llevan todo el peso!

\centering
\includegraphics[scale=0.55]{resampling.pdf}
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Pérdida de diversidad o empobrecimiento}
Tener unas pocas partículas representando una distribución no es muy bueno, pero
tener copias de estas partículas tampoco mejora tanto: a la muestra le falta
diversidad.

\centering
\includegraphics[scale=0.55]{resampling1.pdf}
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Mutación o jittering}
Para mitigar la el empobrecimiento se pueden mover un poco las partículas
para ganar algo de diversidad. Esto puede hacer simulando una trayectoria con MCMC
de manera que se mueva sin perder registro de la verosimilitud.

\centering
\includegraphics[scale=0.5]{jittering.pdf}
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Temperado}
El temperado es la clave del método y busca transformar las partículas del
pronóstico en las del análisis en una sucesión de pasos en lugar de en un solo
paso. Para eso va transformando las partículas de acuerdo a versiones temperadas
de la verosimilitud. \pause

Esto se puede lograr factorizando la verosimilitud como
$
p(\v y_k | \v x_k) = p(\v y_k | \v x_k)^{\phi_1} ... p(\v y_k | \v x_k)^{\phi_n}
$
donde $\phi_1 < ... < \phi_n$ son parámetros de temperado que suman $1$ en total.


\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Temperado}
\centering
\includegraphics[scale=0.7]{tempering.pdf}
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Filtro de partículas temperado}
Para hacer la transformación de una muestra del pronóstico a una muestra de
la distribución del análisis, en el filtro de partículas temperado se los
siguientes pasos para cada $\phi_k$: \pause
\begin{enumerate}
\item Se computan los pesos de cada partícula de acuerdo a
la verosimilitud temperada y se los normaliza. \pause
\item Con los pesos se hace resampleo determinístico basado en transporte
óptimo \pause
\item Se establecen todos los pesos como $1/N$ \pause
\item Se mutan las partículas mediante MCMC con caminata aleatoria
\end{enumerate}

\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Cómo se eligen los $\phi_k$?}
La cantidad de partículas \textit{efectivas} son las que tienen pesos
suficientemente grandes como para contribuir a la representación de la
distribución. Como el temperado cambia cuán ``ancha'' es la verosimilitud,
los pesos, y por ende la cantidad de partículas efectivas, depende de cuan
``ancha'' se haga la verosimilitud. El parámetro de temperado se elige entonces
de manera que el número efectivo de parículas sea igual a cierto número
efectivo de partículas debajo del cual no queremos caer: \pause
$$N_{eff}(\phi_k) = N_{thresh}$$
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Cómo se eligen los $\phi_k$?}

$$N_{eff}(\phi_k) = N_{thresh}$$

Esto es implementado mediante  un algoritmo de bisección sobre el intervalo
$(\phi_{k-1}, 1)$.

Recordemos que el número efectivo de partículas puede ser estimado como
$$N_{eff}(\phi_k) = \frac{1} {\sum_{j=1}^{N} (w^{(j)}(\phi_k))^2} $$
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Algunos resultados}
Comparo el filtro de particulas temperado con el EnKF y un filtro de partículas
sin mutación ni temperado en relación al tamaño de ensamble.

\centering
\includegraphics[scale=0.7]{ensemble_size.eps}
\end{frame}
%---------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
\frametitle{Referencias}
\begin{itemize}
\item Buena referencia general sobre filtros de partículas:
  \begin{itemize}
    \item van Leeuwen, P. J. et al. (2019).
    Particle filters for high-dimensional geoscience applications: A review.
  \end{itemize}
\item El filtro de partículas temperado:
  \begin{itemize}
    \item Dubinkina, S. \& Ruchi, S. (2019). Accounting for model error
    in Tempered Ensemble Transform Particle
    Filter and its application to non-additive model error.
    \item Ruchi, S., Dubinkina, S. \& Iglesias, M. A. (2019).
    Transform-based particle filtering for elliptic Bayesian
    inverse problems.
  \end{itemize}
\item El filtro de partículas ETPF:
  \begin{itemize}
    \item Reich, S. (2013). A Nonparametric Ensemble Transform Method
    for Bayesian Inference.
  \end{itemize}
\item Código utilizado:
  \begin{itemize}
    \item \url{https://gitlab.com/tadeojcocucci/daba_tpf}
  \end{itemize}

\end{itemize}
\end{frame}
%---------------------------------------------------------

\end{document}
