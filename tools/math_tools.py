import numpy as np

def rk4(f, x, t, dt):
    k1 = dt * f(t, x)
    k2 = dt * f(t+dt/2, x+k1/2)
    k3 = dt * f(t+dt/2, x+k2/2)
    k4 = dt * f(t+dt  , x+k3)
    return x + (k1 + 2*(k2 + k3) + k4)/6

def autonomous_rk4(f, x, dt):
    k1 = dt * f(x)
    k2 = dt * f(x+k1/2)
    k3 = dt * f(x+k2/2)
    k4 = dt * f(x+k3)
    return x + (k1 + 2*(k2 + k3) + k4)/6
