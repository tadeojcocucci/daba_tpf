import numpy as np
import scipy.stats as stats

def time_series_RMSE(x, xt):
    return float(np.sqrt(np.mean((x - xt)**2)))

def covered_test(x, P, xt, alpha=0.95):
    nx = x.shape
    s = (xt - x).T @ np.linalg.inv(P) @ (xt - x)
    return s < stats.chi2(df=nx).ppf(alpha)

def time_series_coverage(x, P, xt, alpha=0.95):
    nx, ncy = x.shape
    covered = np.array([covered_test(x[:, i], P[..., i], xt[:, i], alpha=alpha)
                        for i in range(ncy)])
    return np.sum(covered) / float(ncy)

def ensemble_time_series_RMSE(x, xt):
    return time_series_RMSE(np.mean(x, axis=1), xt)

def ensemble_time_series_coverage(x, xt, alpha=0.95):
    nx, ne, ncy = x.shape
    means = np.array([np.mean(x[..., i], axis=1) for i in range(ncy)]).T
    covariances = np.array([np.cov(x[..., i]) for i in range(ncy)]).T
    return time_series_coverage(means, covariances, xt, alpha=alpha)