import numpy as np
import scipy.stats as stats
import copy

from tools import math_tools
from random_variables import RV, GaussianRV

class Process:
    def __init__(self, ncy, X0, f, q, h, r, **kwargs):

        if (f.__annotations__['return'] == Operator and
            f.__annotations__['k'] == int):
            self.f = f
        else:
            raise TypeError("f must be properly annotated")

        if (h.__annotations__['return'] == Operator and
            h.__annotations__['k'] == int):
            self.h = h
        else:
            raise TypeError("h must be properly annotated")

        if isinstance(X0, RV):
            self.X0 = X0
        else:
            raise TypeError("X0 must be of RV class")

        if (r.__annotations__['k'] == int and
            issubclass(r.__annotations__['return'], RV)):
            self.r = r
        else:
            raise TypeError("r must be properly annotated")

        if (q.__annotations__['k'] == int and
            issubclass(q.__annotations__['return'], RV)):
            self.q = q
        else:
            raise TypeError("q must be properly annotated")

        self.ncy = ncy
        self.nx = self.f(0).input_dim
        self.m = self.h(0).output_dim

        for key, value in kwargs.items():
            setattr(self, key, value)

    def likelihood(self, k: int, x, y):
        rv = copy.deepcopy(self.r(k))
        rv.__init__(self.h(k)(x), rv.cov)
        return rv.pdf(y)
    
    def obs_process(self, k: int, x):
        rv = copy.deepcopy(self.r(k))
        rv.__init__(self.h(k)(x), rv.cov)
        return rv

    def transition_process(self, k: int, x):
        rv = copy.deepcopy(self.q(k))
        rv.__init__(self.f(k-1)(x), rv.cov)
        return rv

class Operator:
    def __init__(self, f, n, m):
        self.input_dim =  n
        self.output_dim = m
        self.operator = f

    def __call__(self, *args, **kwargs):
        return self.operator(*args, **kwargs)

class LinearOperator(Operator):
    def __init__(self, A):
        super().__init__(lambda x: self.matrix @ x, A.shape[1], A.shape[0])
        self.matrix = A

    def dot(self, B):
        return self.matrix @ B

def fixed_linear_operator(A):
    def linear_operator(k: int) -> Operator:
        return LinearOperator(A)
    return linear_operator

def operators_from_autonomous_ODE(dxdt, nx, dt, dk, autonomous=True):

    step = lambda x, dt: math_tools.autonomous_rk4(dxdt, x, dt)

    def map_to_operators(k: int) -> Operator:

        def integration(x):
            for i in range(dk):
                x = step(x, dt)
            return x

        return  Operator(integration, nx, nx)

    return map_to_operators

def gaussian_constant_noise(mean, cov):
    def noise(k: int) -> GaussianRV:
        return GaussianRV(mean, cov)
    return noise
