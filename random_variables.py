import numpy as np
import scipy.stats as stats

class RV:
    def sample(self):
        pass
    def pdf(self):
        pass

class GaussianRV(RV):
    def __init__(self, mean=0, cov=1):

        if np.isscalar(mean) and not np.isscalar(cov):
            self.mean = mean*np.ones(cov.shape[0])
            self.cov = cov
        elif np.isscalar(cov) and not np.isscalar(mean):
            self.mean = mean
            self.cov = cov*np.eye(mean.shape[0])
        elif np.isscalar(mean) and np.isscalar(cov):
            self.mean = mean*np.ones(1)
            self.cov = cov*np.eye(1)
        else:
            assert mean.shape[0] == cov.shape[0] == cov.shape[1]
            self.mean = mean
            self.cov = cov

        self.rv = stats.multivariate_normal(
                mean=self.mean, cov=self.cov, allow_singular=True)

    def sample(self, *size):
        return self.rv.rvs(size)

    def pdf(self, x):
        return self.rv.pdf(x)
